;(function (window, document) {

  let rows = random(30)
  const $tbody = document.querySelector('tbody')
  const dummyWord = dummyGenerator(() => String.fromCharCode(range(97, 122)))
  const dummyNumber = dummyGenerator(() => random(10))

  let html = ''

  while (rows--) {
    html += `
      <tr>
        <td>${dummyName()}</td>
        <td>${dummyName()}</td>
        <td>${dummyEmail()}</td>
        <td>${dummyNumber(7, 12)}</td>
      </tr>
    `
  }

  $tbody.innerHTML = html

  function dummyName () {
    return dummyWord(10, 25).replace(/^[a-z]/, letter => letter.toUpperCase())
  }

  function dummyEmail () {
    return dummyWord(10, 15) + '@' + dummyWord(8) + '.com'
  }

  function dummyGenerator (randomGenerator) {
    return (minLength, maxLength) => {
      let string = ''
      let length = range(minLength, maxLength)

      while (length--) {
        string += randomGenerator()
      }

      return string
    }
  }

  function random (length) {
    return Math.floor(Math.random() * length)
  }

  function range (min, max = min) {
    return Math.floor(Math.random() * (max - min + 1)) + min
  }

})(window, window.document)
