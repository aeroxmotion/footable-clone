;(function (window, document) {

  const hiddenColumns = 2
  const mobileSize = 950
  const $table = document.getElementById('table')
  const [$thead, $tbody] = $table.children
  const $rowheaders = $thead.firstElementChild
  const $headers = $rowheaders.children
  const $rows = slice($tbody.children)
  const skipColumns = $headers.length - hiddenColumns
  const $headersToHide = slice($headers, skipColumns)

  const breakpoint = matchMedia('(max-width:' + mobileSize + 'px)')

  breakpoint.addListener(toggleHeaders)

  function toggleHeaders () {
    if (breakpoint.matches) {
      removeColumns($headersToHide, $rowheaders)
    } else {
      addColumns($headersToHide, $rowheaders)
    }
  }

  toggleHeaders()

  $rows.forEach($row => {
    const $columnsToHide = slice($row.children, skipColumns)

    if ($columnsToHide.length) {
      const $indicator = element('span')
      const $dropdown = element('tr')
      const $wrapper = element('td')
      const $subTable = element('table')
      const $container = element('tbody')

      $indicator.className = 'indicator-separation'

      $wrapper.setAttribute('colspan', skipColumns)

      $subTable.className = 'center'

      $subTable.appendChild($container)
      $wrapper.appendChild($subTable)
      $dropdown.appendChild($wrapper)

      const $firstColumn = $row.firstElementChild

      breakpoint.addListener(toggleColumns)

      function toggleColumns () {
        if (breakpoint.matches) return hideColumns()

        addColumns($columnsToHide, $row)

        $firstColumn.removeChild($indicator)
        $tbody.removeChild($dropdown)

        $row.onclick = null
        $row.classList.remove('pointer')
      }

      function hideColumns () {
        removeColumns($columnsToHide, $row)

        $firstColumn.insertBefore($indicator, $firstColumn.firstChild)
        $tbody.insertBefore($dropdown, $row.nextElementSibling)

        $row.onclick = toggleDropdown
        $row.classList.add('pointer')
      }

      function toggleDropdown () {
        $dropdown.classList.toggle('hidden')
        $indicator.textContent = $dropdown.classList.contains('hidden') ? '+' : '-'
      }

      $columnsToHide.forEach(($column, index) => {
        const $item = element('tr')
        const $header = $headersToHide[index].cloneNode(true)

        $header.className = 'left-text'
        $header.textContent += ':'

        $item.appendChild($header)
        $item.appendChild($column.cloneNode(true))

        $container.appendChild($item)
      })

      toggleDropdown()

      if (breakpoint.matches) hideColumns()
    }
  })

  function element (tag) {
    return document.createElement(tag)
  }

  function slice (arrayLike, start) {
    return Array.prototype.slice.call(arrayLike, start)
  }

  function removeColumns ($columns, $row) {
    for ($column of $columns) {
      $row.removeChild($column)
    }
  }

  function addColumns ($columns, $row) {
    for ($column of $columns) {
      $row.appendChild($column)
    }
  }

})(window, window.document)
